const { GraphQLServer } = require('graphql-yoga')
const Binding = require('prisma-binding')
const { prisma } = require('./generated/prisma-client')

const resolvers = require('./resolvers')

const server = new GraphQLServer({
  typeDefs: `${__dirname}/schema.graphql`,
  resolvers,
  context: request => ({
    ...request,
    db: new Binding.Prisma({
      typeDefs: `${__dirname}/generated/graphql-schema/prisma.graphql`,
      endpoint: process.env.PRISMA_ENDPOINT
    }),
    prisma
  })
})

server.start().then(() => console.log('Server runing'))

/*
prisma generate

docker-compose up -d

docker-compose up -d --build --no-deps api

async function main(){

  await prisma.createUser({
    name: 'Kako Ferrare',
    email: 'kakoferrare87@gmail.com',
    password: 'fcm020964'
  })

  const users = await prisma.users()

  console.log('Users: ', users)

}

main().catch(e => console.error(e))*/