const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const { getUserId } =  require('./../utils')

const JWT_SECRET = process.env.JWT_SECRET

function createAccount(_, { description }, ctx, info){
  const UserId = getUserId(ctx)
  return ctx.db.mutation.createAccount({
    data:{
      description,
      user: {
        connect: {
          id: UserId
        }
      }
    }
  }, info)

}

function createCategory(_, { description, operation }, ctx, info){
  const UserId = getUserId(ctx)
  return ctx.db.mutation.createCategory({
    data:{
      description,
      operation,
      user:{
        connect:{
          id: UserId
        }
      }
    }
  }, info)
}

function createRecord(_, args, ctx, info){

  const date = moment(args.date)
  if(!date.isValid()){
    throw new Error('Data inválida!')
  }

  const UserId = getUserId(ctx)
  return ctx.db.mutation.createRecord({
    data: {
      user:{
        connect:{ id: UserId }
      },
      account: {
        connect: { id: args.accountId }
      },
      category: {
        connect: { id: args.categoryId }
      },
      amount: args.amount,
      type: args.type,
      date: args.date,
      description: args.description,
      tags: args.tags,
      note: args.note,
    }
  }, info)
}

async function login(_, { email, password }, ctx, info){
  const user = await ctx.db.query.user({ where: { email } })
  if(!user){
    throw new Error('Credenciais Invalidas!')
  }

  const valida = await bcrypt.compare(password, user.password)
  if(!valida){
    throw new Error('Senha Invalida!')
  }
  const token = jwt.sign({ userId: user.id }, JWT_SECRET, { expiresIn: '2h' })
  return {
    token,
    user
  }
}

async function signup (_, args, ctx, info){

  const password = await bcrypt.hash(args.password, 10)
  const user = await ctx.db.mutation.createUser({ data: { ...args, password } })

  const token = jwt.sign({ userId: user.id }, JWT_SECRET, { expiresIn: '2h' })

  return {
    token,
    user
  }

}

module.exports = {
  createAccount,
  createCategory,
  createRecord,
  login,
  signup
}