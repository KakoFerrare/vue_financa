const moment = require('moment')
const { getUserId } =  require('./../utils')

function accounts(_, args, ctx, info) {
  const UserId = getUserId(ctx)
  return ctx.db.query.accounts({
    where:{
      OR:[
        {
          user: {
            id: UserId
          }
        },
        {
          user: null
        }
      ]
    },
    orderBy: 'description_ASC'
  }, info)
}

function categories(_, { operation }, ctx, info) {
  const UserId = getUserId(ctx)

  let AND = [
    {
      OR:[
        {
          user: { id: UserId } },
        { user: null }
      ]
    }
  ]

  AND = !operation ? AND : [ ...AND, {operation: operation } ]

  return ctx.db.query.categories({
    where: { AND },
    orderBy: 'description_ASC'
  }, info)
}

function totalBalance(_, { date }, ctx, info) {
  const UserId = getUserId(ctx)
  const dateIso = moment(date, 'YYYY-MM-DD').endOf('day').toISOString()
  const mutation = `
  mutation TotalBalance($database: PrismaDatabase, $query: String!) {
    executeRaw(database: $database, query: $query)
  }
  `
  const variables = {
    database: 'default',
    query: `
      SELECT SUM("default$default"."Record"."amount") as totalbalance From "default$default"."Record"
      INNER JOIN "default$default"."_RecordToUser"
      ON "default$default"."_RecordToUser"."A" = "default$default"."Record"."id"
      WHERE "default$default"."_RecordToUser"."B" = '${UserId}'
      AND "default$default"."Record"."date" <= '${dateIso}'
    `
  }
  return ctx.prisma.$graphql(mutation, variables)
  .then(response =>{
    const totalBalance = response.executeRaw[0].totalbalance
    return totalBalance ? totalBalance : 0
  })
}

function records(_, {month,  type, accountsIds, categoriesIds }, ctx, info) {
  
  const UserId = getUserId(ctx)
  let AND = [ { user: {id:UserId } } ]

  AND = !type ? AND : [ ...AND, {type} ]

  AND = !accountsIds || accountsIds.length === 0 ? AND : [ ...AND, { OR: accountsIds.map(id =>({ account:{id }})) } ]

  AND = !categoriesIds || categoriesIds.length === 0 ? AND : [ ...AND, { OR: categoriesIds.map(id =>({ category:{id }})) } ]

  if(month){
    const date = moment(month, 'MM-YYYY')
    const startDate = date.startOf('month').toISOString()
    const endDate = date.endOf('month').toISOString()
    AND = [
      ...AND,
      {date_gte: startDate},
      {date_lte: endDate}
    ]
  }

  return ctx.db.query.records({
    where: { AND },
    orderBy: 'date_ASC'
  }, info)
}

function user(_, args, ctx, info) {
  const UserId = getUserId(ctx)
  return ctx.db.query.user({where:{id: UserId }}, info)
}

module.exports = {
  accounts,
  categories,
  records,
  totalBalance,
  user
}